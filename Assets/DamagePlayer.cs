using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePlayer : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            if(collision.transform.TryGetComponent<IDamageable>(out IDamageable playerLife))
            {
                playerLife.Damage(1);
                GetComponent<IKillable>().Kill();
            }

        }
    }
}
