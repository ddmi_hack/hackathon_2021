using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PowerUP : MonoBehaviour
{
    [SerializeField] private float speed = 0.5f;
    [SerializeField] private AnimationCurve curve;
    Transform player;
    WeaponsSystem playerWeaponsSystem;
    private float _current;

    private void Start()
    {
        playerWeaponsSystem = FindObjectOfType<WeaponsSystem>();
        player = playerWeaponsSystem.transform;
    }

    private void Update()
    {
        _current = Mathf.MoveTowards(_current, 1, speed * Time.deltaTime);
        transform.position = Vector3.Lerp(transform.position, player.position, curve.Evaluate(_current));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            Transform ParticleEffect = transform.GetChild(0);
            ParticleEffect.parent = player;
            ParticleEffect.localPosition = Vector3.zero;
            playerWeaponsSystem.Upgrade();
            AudioSource.PlayClipAtPoint(GetComponent<AudioSource>().clip, transform.position);
            Destroy(gameObject);
        }
    }
}
