using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WeaponsSystem : MonoBehaviour
{
    [SerializeField] private int WeaponTier = 0;
    public GameObject currentWeaponSystem;

    [SerializeField] private List<GameObject> weaponSystems = new List<GameObject>();
    private Queue<GameObject> weapons = new Queue<GameObject>();

    private void Start()
    {
        weapons = new Queue<GameObject>(weaponSystems);
        foreach (var item in weaponSystems)
        {
            item.SetActive(false);
        }
        Upgrade();
    }

    [ContextMenu("Upgrade")]
    public void Upgrade()
    {
        WeaponTier++;
        if (currentWeaponSystem!=null)
        {
            currentWeaponSystem.SetActive(false);
            weapons.Dequeue();
        }
        currentWeaponSystem = weapons.Peek();
        currentWeaponSystem.SetActive(true);
    }
}
