using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateARN : MonoBehaviour
{
    [SerializeField] float degreesPerSecond;

    void Update()
    {
        //transform.Rotate(Vector3.up * degreesPerSecond * Time.deltaTime, Space.Self); 
        transform.Rotate(Vector3.left * degreesPerSecond * Time.deltaTime, Space.Self);
    }
}
