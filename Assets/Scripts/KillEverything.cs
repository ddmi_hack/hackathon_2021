using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class KillEverything : MonoBehaviour
{
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            var damageables = FindObjectsOfType<MonoBehaviour>().OfType<IDamageable>();

            foreach (var item in damageables)
            {
                item.Damage(1);
            }

        }
    }
}
