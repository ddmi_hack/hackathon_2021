using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAnticuerpo : MonoBehaviour
{
    [SerializeField] float degreesPerSecond;

    void Update()
    {
        transform.Rotate(Vector3.back * degreesPerSecond * Time.deltaTime, Space.Self); 
        //transform.Rotate(Vector3.left * degreesPerSecond * Time.deltaTime, Space.Self);
    }
}
