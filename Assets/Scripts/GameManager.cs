using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private bool isPaused = false;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused == false)
            {
                PauseGame();
                isPaused = true;
            }
            else
            {
                isPaused = false;
                ResumeGame();
            }
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            EscenaRecargar();
        }
    }
    public void PauseGame()
    {
        Time.timeScale = 0;
    }
    public void ResumeGame()
    {
        Time.timeScale = 1;
    }
    public void MuteGame()
    {

    }

    #region Scene Selection
    public void EscenaNext()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }
    public void EscenaBack()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);
    }
    public void EscenaCreditos()
    {
        SceneManager.LoadScene("Creditos");
    }
    public void EscenaMenu()
    {
        SceneManager.LoadScene("Main Menu");
        ResumeGame();
    }
    public void EscenaRecargar()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        ResumeGame();
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    #endregion
}
