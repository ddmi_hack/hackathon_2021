using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DeathExplosion : MonoBehaviour, IKillable
{
    public ParticleSystem explosion;

    public void Kill()
    {
        GetComponent<Collider2D>().enabled = false;
        Instantiate(explosion, transform.position, Quaternion.identity);
        transform.DOScale(Vector3.zero, 0.5f)
            .OnComplete(() => Destroy(gameObject));
        transform.DOShakeRotation(0.5f);
    }
}
