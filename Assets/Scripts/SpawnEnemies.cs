using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    public float spawnTime = 1.0f;


    private Vector2 screenBounds;

    // Start is called before the first frame update
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        StartCoroutine(EnemyWave());
    }

    IEnumerator EnemyWave()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnTime);
            spawnEnemy();
        }
    }

    public void spawnEnemy()
    {
        Vector2 position = new Vector2(Random.Range(-screenBounds.x + 1, screenBounds.x - 1), transform.position.y);
        Instantiate(enemy, position, Quaternion.identity);
    }


}
