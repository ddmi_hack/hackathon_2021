using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour, IDamageable
{
    private int health = 8;
    int IDamageable.Health { get => health; set => health = value; }

    public Transform spikes;
    private Queue<Transform> Spikes;

    private void Start()
    {
        Spikes = new Queue<Transform>();
        foreach (Transform item in spikes)
        {
            item.gameObject.SetActive(false);
            Spikes.Enqueue(item);
        }
    }

    public void Damage(int value)
    {
        health -= value;
        if (Spikes.Count!=0)
        {
            Spikes.Dequeue().gameObject.SetActive(true);
        }
        if (health <= 0)
        {
            GetComponent<IKillable>().Kill();
        }

    }
}
