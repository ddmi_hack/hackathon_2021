using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter : MonoBehaviour
{
    Transform bulletSpawnPosition;
    public GameObject bullet;
    [SerializeField] float nextFire = 1.0f;
    [SerializeField] float currentTime = 0.0f;
    void Start()
    {
        bulletSpawnPosition = this.gameObject.transform;
    }
    void Update()
    {
        Disparar();
    }
    void Disparar()
    {
        currentTime += Time.deltaTime;
        if (Input.GetButton("Fire2") && currentTime > nextFire)
        {
            nextFire += currentTime;
            Instantiate(bullet, bulletSpawnPosition.position, Quaternion.identity); //disparar!!
            nextFire -= currentTime;
            AudioSource audioSource = GetComponent<AudioSource>();
            audioSource.pitch = Random.Range(0.8f, 1.5f);
            audioSource.Play();
            currentTime = 0.0f;
        }
    }
}
