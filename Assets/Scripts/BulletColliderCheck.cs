using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletColliderCheck : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.TryGetComponent<IDamageable>(out IDamageable objeto))
        {
            objeto.Damage(1);
            Destroy(gameObject);
        }
    }
}
