using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float moveSpeed = 10.0f;
    Rigidbody2D rb2D;
    void Start()
    {
        rb2D = this.GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        MovePlayer();
    }
    public void MovePlayer()
    {
        rb2D.velocity = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * moveSpeed;
    }
}
