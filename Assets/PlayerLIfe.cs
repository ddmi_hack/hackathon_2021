using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLIfe : MonoBehaviour, IDamageable
{
    private int health = 3;
    int IDamageable.Health { get => health; set => health = value; }

    public void Damage(int value)
    {
        health--;
        FindObjectOfType<HealthManager>().DamageLife();
        GetComponent<AudioSource>().Play();

        if (health < 0)
        {
            GetComponent<IKillable>().Kill();
        }
    }

}
