using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HealthManager : MonoBehaviour
{
    private Queue<Transform> health;

    private void Start()
    {
        health = new Queue<Transform>();
        foreach (Transform item in transform)
        {
            health.Enqueue(item);
        }
    }

    public void DamageLife()
    {
        if (health.Count != 0)
        {
            GameObject healthIcon = health.Dequeue().gameObject;
            Sequence healthIconSequence = DOTween.Sequence();

            healthIconSequence
                .Append(healthIcon.transform.DOPunchScale(new Vector3(0.7f, 0.7f, 0.7f), 0.5f))
                .Append(healthIcon.transform.DOScale(0, 0.3f))
                .OnComplete(()=> healthIcon.SetActive(true));
        }
        else
        {
            FindObjectOfType<GameplaySceneLoader>().LostGame();
        }
    }
}
