using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplaySceneLoader : MonoBehaviour
{
    public List<string> SceneNames;

    private void Start()
    {
        foreach (string item in SceneNames)
        {
            SceneManager.LoadScene(item, LoadSceneMode.Additive);
        }
    }

    public void WinGame()
    {
        StartCoroutine(LoadScene("Victory"));
    }

    public void LostGame()
    {
        StartCoroutine(LoadScene("Defeat"));
    }

    IEnumerator LoadScene(string sceneName)
    {
        yield return new WaitForSeconds(0.8f);
        SceneManager.LoadScene(sceneName);
    }
}
